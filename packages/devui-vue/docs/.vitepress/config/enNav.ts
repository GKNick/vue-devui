const enNav = [
  { text: 'Component', link: '/en-US/' },
  { text: 'Version history', link: 'https://gitee.com/devui/vue-devui/releases' },
  { text: 'Design disciplines', link: 'https://devui.design/design-cn/start' },
]
  
  export default enNav